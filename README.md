Code for extracting segmentation, cell line, and division data from quantified
data from segmented images. Currently tailored to suit the Willis et al. (2016)
paper, but is adaptable to similarly structured data sets with little 
modification.

Documentation in the making.